from django.shortcuts import render, redirect, get_object_or_404

# Create your views here.

# "from" allows the importation of necessary classes/modules, methods, and other files needed in our application from the django.http package; "import" defines what we are importing from the package
from django.http import HttpResponse # Functions similarly to response.send in express

# Local imports
from .models import ToDoItem, Events
from .forms import LoginForm, AddTaskForm, UpdateTaskForm, RegisterForm, AddEventForm, UpdateEventForm, UpdateProfileForm

from django.utils import timezone

# Importing built-in User Model
from django.contrib.auth.models import User

# Authenticate function
from django.contrib.auth import authenticate, login, logout

from django.forms.models import model_to_dict

# To use the template we created
# from django.template import loader

def index(request):
	todoitem_list = ToDoItem.objects.filter(user_id = request.user.id)
	events_list = Events.objects.filter(user_id = request.user.id)
	# template = loader.get_template('index.html')
	context = {
		'todoitem_list': todoitem_list,
		'events_list': events_list,
		'user': request.user
	}
	# join() is the counterpart to concat() in js
	# output = ", ".join([todoitem.task_name for todoitem in todoitem_list])

	# render(request, route_template, context)

	return render(request, "index.html", context)

def todoitem(request, todoitem_id):
	# response = f"You are viewing the details of {todoitem_id}."
	# return HttpResponse(response)

	todoitem = get_object_or_404(ToDoItem, pk = todoitem_id)

	return render(request, "todoitem.html", model_to_dict(todoitem))

def event(request, event_id):
	# response = f"You are viewing the details of {todoitem_id}."
	# return HttpResponse(response)

	event = get_object_or_404(Events, pk = event_id)

	return render(request, "event.html", model_to_dict(event))

# This function is responsible for registering on our application
def register(request):
	
	context = {}
	users = User.objects.all()
	is_user_registered = False
	success = False

	if request.method == "POST":

		form = RegisterForm(request.POST)

		if form.is_valid() == False:
			form = RegisterForm()

		else:

			# user_pass = form.cleaned_data['password']

			user = User()

			user.username = form.cleaned_data['username']
			user.set_password(form.cleaned_data['password'])
			user.first_name = form.cleaned_data['first_name']
			user.last_name = form.cleaned_data['last_name']
			user.email = form.cleaned_data['email']
			user.is_staff = False
			user.is_active = True

			for indiv_user in users:
				if indiv_user.username == user.username:
					is_user_registered = True
					break

			if is_user_registered == False:
				# To save our user
				user.save()
				success = True

			context = {
				'is_user_registered': is_user_registered,
				'first_name': user.first_name,
				'last_name': user.last_name,
				'success': success
			}

	return render(request, "register.html", context)



	# users = User.objects.all()
	# is_user_registered = False

	# user = User()
	# user.username = "janedoe"
	# user.first_name = "jane"
	# user.last_name = "doe"
	# user.email = "jane@mail.com"
	# user.set_password("jane1234") # Using this will automatically hash the password in our database.
	# user.is_staff = False
	# user.is_active = True

	# for indiv_user in users:
	# 	if indiv_user.username == user.username:
	# 		is_user_registered = True
	# 		break # We only need to check once if the username is taken and we're only changing the value of is_user_registered to true for our conditional rendering, hence the break.

	# if is_user_registered == False:
	# 	# To save our user
	# 	user.save()

	# context = {
	# 	'is_user_registered': is_user_registered,
	# 	'first_name': user.first_name,
	# 	'last_name': user.last_name
	# }

	# return render(request, "register.html", context)

# def change_password(request):

# 	is_user_authenticated = False

# 	user = authenticate(username = "johndoe", password = "johndoe12345")
# 	# If there is a user with the given credentials, authenticate() will return an object containing the credentials; if not, user's value will be None (literally None)

# 	if user is not None:
# 		authenticated_user = User.objects.get(username = 'johndoe')
# 		authenticated_user.set_password("johndoe123456")
# 		authenticated_user.save()

# 		is_user_authenticated = True

# 	context = {
# 		"is_user_authenticated": is_user_authenticated
# 	}

# 	return render(request, "change_password.html", context)

def login_user(request):

	context = {}

	# If this is a post request, we will need to process the form data.
	if request.method == "POST":

		form = LoginForm(request.POST)

		# is_valid() checks if the captured data from the form follows the provided rules; in this case it will check if the value exceeds 20 characters or not. It returns a Boolean value.
		if form.is_valid() == False:
			form = LoginForm()
		else:
			# You can't directly extract the username from the form; first, you must use cleaned_data. cleaned_data retrieves the information from the form.
			username = form.cleaned_data['username']
			password = form.cleaned_data['password']

			user = authenticate(username = username, password = password)

			if user is not None:
				context = {
					'username': username,
					'password': password
				}
				login(request, user)
				return redirect("todolist:index")

			else:
				context = {
					'error': True
				}
				print(user)

	
	return render(request, "login.html", context)

def logout_user(request):
	logout(request)
	return redirect("todolist:index")

def add_task(request):

	context = {}

	if request.method == "POST":
		form = AddTaskForm(request.POST)

		if form.is_valid() == False:
			form = AddTaskForm()

		else:
			task_name = form.cleaned_data['task_name']
			description = form.cleaned_data['description']

			duplicates = ToDoItem.objects.filter(task_name = task_name, user_id = request.user.id)

			if not duplicates:
				
				# Create an object based on the ToDoItem model and save it to the database
				ToDoItem.objects.create(task_name = task_name, description = description, date_created = timezone.now(), user_id = request.user.id)

				return redirect("todolist:index")
			else:
				context = {
					'error': True
				}


	return render(request, "add_task.html", context)

def add_event(request):

	context = {}

	if request.method == "POST":
		form = AddEventForm(request.POST)

		if form.is_valid() == False:
			form = AddEventForm()

		else:
			event_name = form.cleaned_data['event_name']
			description = form.cleaned_data['description']
			event_date = form.cleaned_data['event_date']

			duplicates = Events.objects.filter(event_name = event_name, user_id = request.user.id, status = 'pending')

			if not duplicates:

				Events.objects.create(event_name = event_name, description = description, event_date = event_date, user_id = request.user.id)

				return redirect('todolist:index')
			else:
				context = {
					'error': True
				}

	return render(request, "add_event.html", context)

def update_task(request, todoitem_id):

	todoitem = ToDoItem.objects.filter(pk = todoitem_id)

	context = {
		'user': request.user,
		'todoitem_id': todoitem_id,
		'task_name': todoitem[0].task_name,
		'description': todoitem[0].description,
		'status': todoitem[0].status
	}

	if request.method == "POST":
		form = UpdateTaskForm(request.POST)

		if form.is_valid() == False:
			form = UpdateTaskForm()
		
		else:
			task_name = form.cleaned_data['task_name']
			description = form.cleaned_data['description']
			status = form.cleaned_data['status']

			if todoitem:

				todoitem[0].task_name = task_name
				todoitem[0].description = description
				todoitem[0].status = status

				todoitem[0].save()

				return redirect("todolist:viewtodoitem", todoitem_id=todoitem_id)

			else:

				context = {
					'error': True
				}

	return render(request, 'update_task.html', context)

def update_event(request, event_id):

	event = Events.objects.filter(pk = event_id)

	context = {
		'user': request.user,
		'event_id': event_id,
		'event_name': event[0].event_name,
		'description': event[0].description,
		'status': event[0].status,
		'event_date': event[0].event_date
	}

	if request.method == "POST":
		form = UpdateEventForm(request.POST)

		if form.is_valid() == False:
			form = UpdateEventForm()
		
		else:
			event_name = form.cleaned_data['event_name']
			description = form.cleaned_data['description']
			status = form.cleaned_data['status']
			event_date = form.cleaned_data['event_date']

			if event:

				event[0].event_name = event_name
				event[0].description = description
				event[0].status = status
				event[0].event_date = event_date

				event[0].save()

				return redirect("todolist:viewevent", event_id=event_id)

			else:

				context = {
					'error': True
				}

	return render(request, 'update_event.html', context)

def delete_task(request, todoitem_id):

	todoitem = ToDoItem.objects.filter(pk = todoitem_id).delete()

	return redirect('todolist:index')

def delete_event(request, event_id):

	event = Events.objects.filter(pk = event_id).delete()

	return redirect('todolist:index')

def update_profile(request):

	context = {}
	success = False

	if request.method == "POST":

		user = User.objects.get(username = request.user.username)
		form = UpdateProfileForm(request.POST)

		if form.is_valid() == False:
			form = UpdateProfileForm()

		else:

			newpass = form.cleaned_data['password']

			user.set_password(newpass)
			user.first_name = form.cleaned_data['first_name']
			user.last_name = form.cleaned_data['last_name']

			user.save()
			success = True

		context = {
			'success': success
		}


	return render(request, "update_profile.html", context)