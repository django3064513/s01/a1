from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class ToDoItem(models.Model):
	task_name = models.CharField(max_length = 50)
	description = models.CharField(max_length = 200)
	status = models.CharField(max_length = 50, default = "pending")
	date_created = models.DateTimeField('date created') # An example of a "verbose name"
	user = models.ForeignKey(User, on_delete = models.RESTRICT, default = "")

class Events(models.Model):
	event_name = models.CharField(max_length = 50)
	description = models.CharField(max_length = 200)
	status = models.CharField(max_length = 50, default = "pending")
	event_date = models.CharField(max_length = 30)
	user = models.ForeignKey(User, on_delete = models.RESTRICT, default = "")