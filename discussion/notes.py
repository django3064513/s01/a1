# Recall: Four pillars of Python OOP

# CharField is the same as VARCHAR

# [Section] Each model is represented by a class that inherits properties from django.db.models.Model.
# Each field is represented by an instance of a field class (e.g. CharField, which is for number of characters in character fields; DateTimeField for date-times; they tell Django what type of data each field holds)
# Some field classes have required arguments. CharField requires that you provide a max_length, for instance.
# A field can also have various optional arguments; for example, we set the default value of status to "pending".

# [Section] Migration
# Syntax:
	# python manage.py makemigrations todolist
# By running makemigrations, you are telling Django that you've made some changes to your models.

# Migrate Commands
	# python manage.py sqlmigrate todolist 0001 (declaring migrations to be SQL)
	# python manage.py migrate

# You can view the contents of db.sqlite3 on at sqliteonline.com if you don't have a compiler

# To run the Python shell:
# python manage.py shell
# Be careful not to have typos when typing in the shell! Try to type your syntax elsewhere to avoid accidents.

todoitem = ToDoItem(task_name = "Eat", description = "Dinner", date_created = timezone.now());
# Saving
todoitem.save();

# To connect MySQL
# pip install mysql_connector
# pip install mysql

# OR

# pip install mysql_connector mysql

# Creating a Superuser
# MacOS/Linux
python3 manage.py createsuperuser
# Windows
winpty python manage.py createsuperuser