from django.shortcuts import render, redirect
from django.http import HttpResponse

from .models import GroceryItem
from django.template import loader

# Importing built-in User Model
from django.contrib.auth.models import User

# Authenticate function
from django.contrib.auth import authenticate, login, logout

# Query set to dictionary format
from django.forms.models import model_to_dict

def index(request) :
	grocery_item_list = GroceryItem.objects.all()

	context = {
		'grocery_item_list': grocery_item_list,
		'user': request.user
	}
	return render(request, "index.html", context)

def grocery_item(request, grocery_item_id):
	# response = f"Here are the details for Grocery Item #{grocery_item_id}:"
	# return HttpResponse(response)

	grocery_item = model_to_dict(GroceryItem.objects.get(pk = grocery_item_id))

	return render(request, 'grocery_item.html', grocery_item)

def register(request):
	
	users = User.objects.all()
	is_user_registered = False

	user = User()
	user.username = "cloudstrife"
	user.first_name = "Cloud"
	user.last_name = "Strife"
	user.email = "cloud@mail.com"
	user.set_password("cloud1234") # Using this will automatically hash the password in our database.
	user.is_staff = False
	user.is_active = True

	for indiv_user in users:
		if indiv_user.username == user.username:
			is_user_registered = True
			break # We only need to check once if the username is taken and we're only changing the value of is_user_registered to true for our conditional rendering, hence the break.

	if is_user_registered == False:
		# To save our user
		user.save()

	context = {
		'is_user_registered': is_user_registered,
		'first_name': user.first_name,
		'last_name': user.last_name
	}

	return render(request, "register.html", context)

def change_password(request):

	is_user_authenticated = False

	user = authenticate(username = "cloudstrife", password = "cloud1234")

	if user is not None:
		authenticated_user = User.objects.get(username = "cloudstrife")
		authenticated_user.set_password("tifa1234")
		authenticated_user.save()

		is_user_authenticated = True

	context = {
		'is_user_authenticated': is_user_authenticated
	}

	return render(request, 'change_password.html', context)

def login_user(request):

	username = 'cloudstrife'
	password = 'tifa1234'

	user = authenticate(username = username, password = password)

	if user is not None:
		login(request, user)
		return redirect('index')

	else:
		return render(request, 'login.html')

def logout_user(request):

	logout(request)
	return redirect('index')