from django.urls import path
from . import views

urlpatterns = [
	path('index', views.index, name = 'index'),
	path('<int:grocery_item_id>', views.grocery_item, name = 'viewgrocery_item'),
	path('register', views.register, name = 'register'),
	path('change_password', views.change_password, name = 'change_password'),
	path('login', views.login_user, name = 'login'),
	path('logout', views.logout_user, name = 'logout')
]